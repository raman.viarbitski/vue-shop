import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import ItemDetails from "./views/ItemDetails";
import CreateItem from "./views/CreateItem";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/create",
      name: "create",
      component: CreateItem
    },
    {
      path: "/item/:id",
      name: "itemDetails",
      component: ItemDetails
    },
    {
      path: "/cart",
      name: "cart",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Cart.vue")
    }
  ]
});
