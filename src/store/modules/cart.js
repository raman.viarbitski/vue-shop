const ADD_ITEM = "ADD_ITEM";
const INCREMENT_QUANTITY = "INCREMENT_QUANTITY";
const REMOVE_ITEM = "REMOVE_ITEM";
const CHECKOUT = "CHECKOUT";

const state = {
  items: []
};

const getters = {
  totalQuantity: state =>
    state.items.reduce((prev, current) => {
      prev += current.quantity;
      return prev;
    }, 0),

  cartItems: (state, getters, rootState) => {
    return state.items.map(cartItem => {
      const item = rootState.catalog.items.find(
        catalogItem => catalogItem.id === cartItem.id
      );
      return {
        id: item.id,
        name: item.name,
        price: item.price,
        quantity: cartItem.quantity
      };
    });
  },

  totalPrice: (state, getters) => {
    const price = getters.cartItems.reduce((prev, current) => {
      prev += +current.price;
      return prev;
    }, 0);
    return price.toFixed(2);
  }
};

const actions = {
  checkout({ commit }) {
    commit(CHECKOUT);
    window.alert("Thank you! \n Your order has been sent for processing");
  },

  addItemToCart({ state, commit }, newItem) {
    const cartItem = state.items.find(item => item.id === newItem.id);
    if (!cartItem) {
      commit(ADD_ITEM, newItem);
    } else {
      commit(INCREMENT_QUANTITY, cartItem);
    }
  },

  removeItemFromCart({ commit }, item) {
    commit(REMOVE_ITEM, item);
  }
};

const mutations = {
  [CHECKOUT](state) {
    state.items = [];
  },

  [ADD_ITEM](state, { id }) {
    state.items = [...state.items, { id, quantity: 1 }];
  },

  [INCREMENT_QUANTITY](state, cartItem) {
    cartItem.quantity += 1;
  },

  [REMOVE_ITEM](state, { id }) {
    state.items = state.items.filter(item => item.id !== id);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
