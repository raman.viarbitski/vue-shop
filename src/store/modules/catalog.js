import { categories, colors, products as items } from "../initialState";

const ADD_ITEM = "ADD_ITEM";

const state = {
  items,
  categories,
  colors,
  isLoading: false,
  filters: {
    keyword: "",
    categories: [],
    colors: [],
    availableOnly: false
  }
};

const getters = {
  getItemById: state => id => state.items.find(item => item.id === id),
  filteredItems: state => {
    const { keyword, categories, colors, availableOnly } = state.filters;
    return state.items.filter(item => {
      return (
        (!keyword.length ||
          item.name.toLowerCase().includes(keyword.toLowerCase())) &&
        (!categories.length || categories.includes(item.category)) &&
        (!colors.length || colors.includes(item.color)) &&
        (!availableOnly || item.isAvailable)
      );
    });
  }
};

const actions = {
  addItem({ commit }, payload) {
    commit(ADD_ITEM, payload);
  }
};

const mutations = {
  updateFilter(state, { filterName, value }) {
    state.filters[filterName] = value;
  },

  [ADD_ITEM](state, payload) {
    state.items.push(payload);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
