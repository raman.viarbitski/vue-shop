export const products = [
  {
    id: "5c005e7cdffa480122966c02",
    name: "occaecat commodo",
    category: "Dresses",
    price: "324.99",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Exercitation magna incididunt irure dolor ut aliquip aliquip. Reprehenderit dolore magna eu laborum in sit culpa proident elit do. Est occaecat anim reprehenderit aute veniam commodo non reprehenderit magna ad minim. Aliquip velit enim irure veniam. Culpa cillum magna ipsum irure ipsum proident aliqua. Exercitation excepteur duis tempor adipisicing in est voluptate proident laborum ut aliquip.",
    isAvailable: true
  },
  {
    id: "5c005e7c9e913039f8bb7aca",
    name: "ea eu",
    category: "Shirts",
    price: "137.29",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Cillum veniam qui proident aliqua nulla ullamco pariatur sint cupidatat et in. Duis do qui aute aute dolore commodo ad. Nostrud dolore sit quis qui. Fugiat sit ut labore eiusmod ipsum et reprehenderit cupidatat eu aliquip. Et aliquip deserunt ut consectetur ex ex mollit amet consequat proident. Aliquip id ullamco cillum enim laborum ullamco qui cupidatat tempor sit.",
    isAvailable: true
  },
  {
    id: "5c005e7cc7acee4a3c6a48f4",
    name: "commodo irure",
    category: "Bags",
    price: "983.68",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "red",
    description:
      "Et dolor quis reprehenderit ipsum ullamco non esse enim qui pariatur anim laborum. Ad consectetur duis dolore id eu dolore mollit reprehenderit. Amet officia nisi consectetur est cillum nulla reprehenderit dolor. Aliquip ad aute ipsum voluptate. Reprehenderit non do fugiat amet commodo commodo adipisicing ex mollit. Occaecat aute sint ut culpa et do amet magna anim nostrud commodo. Ipsum esse minim nisi eu consequat ullamco dolore id incididunt minim do ipsum Lorem.",
    isAvailable: true
  },
  {
    id: "5c005e7cd086c31e8b5bca54",
    name: "in quis",
    category: "Footwear",
    price: "714.45",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "green",
    description:
      "Commodo fugiat cupidatat est consectetur est mollit. Sit veniam sint id eiusmod. Qui irure amet officia tempor quis voluptate culpa minim adipisicing tempor sunt. Esse sit irure amet aute veniam non Lorem qui ullamco ipsum culpa.",
    isAvailable: true
  },
  {
    id: "5c005e7cf3b210c5e0bea5cf",
    name: "tempor ex",
    category: "Footwear",
    price: "951.61",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Ea laborum ut eu non in labore veniam. Do anim exercitation nostrud nulla exercitation ea enim nostrud in amet consequat dolor mollit nisi. Voluptate sunt ad culpa proident amet. Aliqua tempor ea deserunt est velit aliqua ullamco. Qui anim sit ut veniam deserunt proident commodo eu. Incididunt est dolore aliqua aliquip sunt irure nisi Lorem sit officia. Mollit in mollit tempor ullamco irure dolore occaecat occaecat.",
    isAvailable: false
  },
  {
    id: "5c005e7c7e694b514d3b17c7",
    name: "duis quis",
    category: "Footwear",
    price: "652.88",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Labore quis qui pariatur ullamco ullamco cupidatat reprehenderit culpa. Anim culpa sint in aliqua mollit non culpa. Est cupidatat nisi occaecat ad excepteur enim incididunt incididunt nulla consectetur incididunt commodo nostrud. Commodo non Lorem consectetur exercitation deserunt aute. Enim veniam in irure officia nulla esse sint cillum. Ex Lorem esse officia elit adipisicing in exercitation ea deserunt.",
    isAvailable: false
  },
  {
    id: "5c005e7c00192ee4f86fafa7",
    name: "id do",
    category: "Bags",
    price: "573.99",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Ea elit velit dolor excepteur ut sunt eiusmod. Veniam aliquip amet et ullamco incididunt. Nulla ex nulla quis nulla incididunt pariatur voluptate sit sit qui pariatur in minim. In consectetur qui laboris nisi eu voluptate.",
    isAvailable: true
  },
  {
    id: "5c005e7cf65082e8ad60a914",
    name: "ad adipisicing",
    category: "Dresses",
    price: 614,
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Laboris nostrud reprehenderit qui tempor culpa. Fugiat consequat dolore excepteur aliquip eiusmod et excepteur ullamco. Ad est anim eiusmod incididunt ipsum. Elit aute duis reprehenderit dolore magna ut fugiat adipisicing pariatur pariatur dolor veniam.",
    isAvailable: true
  },
  {
    id: "5c005e7cbbd70a1ee6694f9e",
    name: "ex est",
    category: "Dresses",
    price: "836.58",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "red",
    description:
      "Ipsum aliqua anim ea fugiat proident anim qui consectetur. Elit minim id sit laboris et cillum occaecat qui cillum. Culpa proident nulla reprehenderit officia amet minim occaecat amet irure dolor esse consequat voluptate enim. Minim officia do ut labore id cupidatat ut cupidatat amet irure dolor et id laboris. Excepteur fugiat tempor aute ex voluptate nostrud adipisicing cupidatat nostrud ipsum ullamco. Voluptate excepteur enim esse excepteur pariatur cillum amet occaecat. Velit ea occaecat ad veniam laboris consequat voluptate sint irure est Lorem.",
    isAvailable: true
  },
  {
    id: "5c005e7c7dd9a7f7128bb479",
    name: "elit dolor",
    category: "Dresses",
    price: "789.27",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "brown",
    description:
      "Aliqua aute nisi Lorem eiusmod officia Lorem cupidatat id excepteur non ullamco. Occaecat velit anim laborum do exercitation ea in velit ea. Ex adipisicing officia enim laboris ut incididunt deserunt nostrud eiusmod irure esse.",
    isAvailable: false
  },
  {
    id: "5c005e7c46e4e95fcf29b9e0",
    name: "cillum voluptate",
    category: "Footwear",
    price: "920.85",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "brown",
    description:
      "Excepteur velit laborum duis minim voluptate laborum dolor consectetur. Do in mollit incididunt laboris ea reprehenderit non mollit id amet aliquip. Nisi ut magna incididunt non eu. Id minim culpa consectetur fugiat reprehenderit aliquip velit est consectetur. Labore in est magna do ipsum culpa quis adipisicing ea laboris.",
    isAvailable: true
  },
  {
    id: "5c005e7c7ff03430564dea98",
    name: "aliqua fugiat",
    category: "Footwear",
    price: "941.45",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "red",
    description:
      "Sit mollit ut excepteur sint cillum tempor. Dolore irure fugiat fugiat ipsum cupidatat sunt. Id amet excepteur aliquip ut commodo laborum non nostrud ut cupidatat nostrud laboris consectetur. Voluptate exercitation nulla pariatur incididunt est ut. Esse anim est amet nulla id in irure aliquip amet esse Lorem. Mollit non magna sit elit enim quis minim velit. Excepteur ex proident Lorem eiusmod qui.",
    isAvailable: false
  },
  {
    id: "5c005e7cba5dcd00fa57c04a",
    name: "anim amet",
    category: "Bags",
    price: "950.65",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "green",
    description:
      "Quis dolore cupidatat duis laborum in nulla. Aliquip esse fugiat non cillum labore ullamco esse magna consectetur voluptate. Culpa culpa id reprehenderit excepteur consectetur sint magna. Exercitation ut nulla qui sunt eu pariatur irure veniam nulla magna id dolor. Sint in commodo reprehenderit in proident ex. Labore officia eiusmod ad id tempor veniam Lorem. Quis do in est enim anim mollit est dolore ex adipisicing enim aliquip id.",
    isAvailable: false
  },
  {
    id: "5c005e7c5ff24defd368d1d2",
    name: "labore qui",
    category: "Dresses",
    price: "501.09",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Aute quis Lorem aliqua pariatur enim ea non magna deserunt dolor. Duis est aliquip dolor aute dolore laboris eu deserunt id sint. Sint eu veniam mollit proident sunt fugiat elit laborum aute reprehenderit. Nisi mollit aute occaecat ut cillum eu incididunt ad nostrud labore fugiat. Ipsum excepteur veniam et mollit officia quis eiusmod nulla eu consectetur. Commodo ullamco officia Lorem quis laborum esse enim.",
    isAvailable: false
  },
  {
    id: "5c005e7c8c54e481fb125667",
    name: "commodo do",
    category: "Bags",
    price: "588.99",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "green",
    description:
      "Aliqua aute labore qui irure cupidatat. Ad excepteur non exercitation ea cupidatat nulla sint ut. Sint dolor et nostrud ullamco proident sunt.",
    isAvailable: false
  },
  {
    id: "5c005e7c11acc825dc49ff74",
    name: "aliqua qui",
    category: "Dresses",
    price: "723.62",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "red",
    description:
      "Officia culpa consectetur veniam ullamco elit. Laboris cillum ut aliqua exercitation. Deserunt officia labore qui minim fugiat culpa ullamco commodo aliquip ad in id incididunt ea. Laboris officia officia voluptate ad ipsum enim sit quis et quis.",
    isAvailable: true
  },
  {
    id: "5c005e7c616be3444f7d1429",
    name: "fugiat quis",
    category: "Shirts",
    price: "761.33",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "blue",
    description:
      "Esse excepteur aute sunt eiusmod laboris officia duis ipsum. Et id nostrud aute consectetur ex ad anim non voluptate dolore non cillum Lorem. Reprehenderit non reprehenderit Lorem pariatur eiusmod aliquip et non amet minim mollit exercitation. Magna aute nulla sit deserunt duis ad commodo deserunt consectetur consequat. Occaecat anim excepteur amet veniam consectetur non sit minim laborum laborum labore. Est aute pariatur ad duis Lorem exercitation aliquip deserunt ut aute voluptate minim amet dolore.",
    isAvailable: true
  },
  {
    id: "5c005e7c3bd0222e2b8a442e",
    name: "minim enim",
    category: "Bags",
    price: "462.19",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "red",
    description:
      "Occaecat quis voluptate ad laborum dolor occaecat anim in veniam deserunt labore mollit ut fugiat. Exercitation cillum minim consequat commodo. Sint excepteur sunt quis laborum quis duis incididunt minim ex laborum dolore cillum. Incididunt ad proident occaecat anim cupidatat dolore pariatur commodo amet dolor fugiat commodo amet. Aliquip fugiat dolore voluptate commodo adipisicing dolore sunt. Cupidatat occaecat eiusmod est ea laborum nisi tempor et tempor laborum nostrud. Deserunt amet cillum ipsum eu magna.",
    isAvailable: true
  },
  {
    id: "5c005e7c612f1922293fd3c5",
    name: "quis dolore",
    category: "Dresses",
    price: "238.28",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "red",
    description:
      "Proident nisi do deserunt elit eiusmod non exercitation. Pariatur incididunt et cupidatat nisi in. Elit dolor laboris exercitation non nisi duis fugiat sit reprehenderit esse qui officia aliquip labore.",
    isAvailable: false
  },
  {
    id: "5c005e7c050969e5910e18ed",
    name: "nulla cupidatat",
    category: "Shirts",
    price: "969.12",
    currencySymbol: "$",
    image: "http://placehold.it/150x150",
    color: "brown",
    description:
      "Non labore sit reprehenderit pariatur non ex cupidatat et irure. Est voluptate aliqua sint magna veniam amet cillum consectetur sint ullamco ullamco aute cillum. Ex non est eu sunt pariatur aliqua.",
    isAvailable: true
  }
];

export const categories = ["Bags", "Dresses", "Shirts", "Footwear"];
export const colors = ["blue", "brown", "green", "red"];
